import './assets/main.css'
// 等宽字体
import 'vfonts/FiraCode.css'

import {createApp} from 'vue'
import App from './App.vue'
import naive from 'naive-ui'

let app = createApp(App);
console.log(' HI: ',  import.meta.env.VITE_APP_API_URL);
app.use(naive)
app.mount('#app')