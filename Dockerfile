# 第一阶段：构建阶段
FROM node:20.11.0-alpine as build-stage

# 设置工作目录
WORKDIR /app

# 复制 package.json 和 package-lock.json 文件到工作目录
COPY package*.json ./

RUN npm config set registry https://registry.npmmirror.com

# 安装依赖
RUN npm install

# 复制所有文件到工作目录
COPY . .

# 执行构建命令
RUN npm run build

# 第二阶段：生产阶段
FROM nginx:1.21-alpine as production-stage

# 将构建阶段生成的静态文件复制到 Nginx 的默认静态文件目录
COPY --from=build-stage /app/dist /usr/share/nginx/html

# 复制自定义的 Nginx 配置文件到容器中
COPY nginx.conf /etc/nginx/conf.d/default.conf

# 暴露容器的 80 端口
EXPOSE 80

# 启动 Nginx 服务
CMD ["nginx", "-g", "daemon off;"]